# JobAd Links deployments

## Current configuration is messy:

| Openshift cluster | Openshift namespace          | ArgoCD Infra                                     | overlay in joblinks-api-infra |
|-------------------|------------------------------|--------------------------------------------------|-------------------------------|
| onprem-test       | joblinks-api-dev-onprem-test | onprem-test/joblinks-api-develop.yaml            | onprem-test                   |
| onprem-test       | joblinks-search-api-onprem   | onprem-test/joblinks-search-api-onprem-test.yaml | onprem                        |

## New testing deployments after conversion to OpenSearch:

| Openshift cluster | Openshift namespace  | ArgoCD Infra                          | overlay in joblinks-api-infra |
|-------------------|----------------------|---------------------------------------|-------------------------------|
| onprem-test       | joblinks-api-develop | onprem-test/joblinks-api-develop.yaml | develop-onprem-test           | 
| onprem-test       | joblinks-api-staging | onprem-test/joblinks-api-staging.yaml | staging-onprem-test           | 

## New production deployment after conversion to OpenSearch:

| Openshift cluster | Openshift namespace | ArgoCD Infra                       | overlay in joblinks-api-infra |
|-------------------|---------------------|------------------------------------|-------------------------------|
| onprem-prod       | joblinks-api-prod   | onprem-prod/joblinks-api-prod.yaml | prod-onprem-prod              | 

# JobSearch deployments

## New API deployments after conversion to OpenSearch:

| Openshift cluster | Openshift namespace      | ArgoCD Infra                           | overlay in jobsearch-api-infra |
|-------------------|--------------------------|----------------------------------------|--------------------------------|
| onprem-test       | jobsearch-api-develop    | onprem-test/jobsearch-api-develop.yaml | develop-onprem-test            |
| onprem-test       | jobsearch-api-staging    | onprem-test/jobsearch-api-staging.yaml | staging-onprem-test            |
| onprem-test       | jobsearch-api-i1         | onprem-test/jobsearch-api-i1.yaml      | i1-onprem-test                 |
| onprem-test       | jobsearch-api-t2         | onprem-test/jobsearch-api-t2.yaml      | t2-onprem-test                 |
| onprem-test       | jobsearch-historical-api | onprem-test/jobsearch-historical.yaml  | historical-onprem-test         |
| onprem-test       | jobsearch-api-t2         | onprem-test/jobsearch-api-t2.yaml      | t2-onprem-test                 |

## New IMPORTER deployments after conversion to OpenSearch:

| Openshift cluster | Openshift namespace           | ArgoCD Infra                                   | overlay in jobsearch-importers-infra |
|-------------------|-------------------------------|------------------------------------------------|--------------------------------------|
| onprem-test       | jobsearch-importer-develop    | onprem-test/jobsearch-importer-develop.yaml    | develop-importer-onprem-test         |
| onprem-test       | jobsearch-importer-staging    | onprem-test/jobsearch-importer-staging.yaml    | staging-importer-onprem-test         |
| onprem-test       | jobsearch-importer-i1         | onprem-test/jobsearch-importer-i1.yaml         | i1-importer-onprem-test              |
| onprem-test       | jobsearch-importer-t2         | onprem-test/jobsearch-importer-t2.yaml         | t2-importer-onprem-test              |
| onprem-test       | jobsearch-historical-importer | onprem-test/jobsearch-historical-importer.yaml | historical-importer-onprem-test      |

## Production
### API
| Openshift cluster | Openshift namespace           | ArgoCD Infra                        | overlay in jobsearch-api-infra |
|-------------------|-------------------------------|-------------------------------------|--------------------------------|
| onprem-prod       | jobsearch-api            | onprem-prod/jobsearch-api-prod.yaml | onprem-prod               |
| onprem-prod       | jobsearch-historical-api | onprem-prod/jobsearch-api-prod.yaml | historical-onprem-prod    |

### Import
| Openshift cluster           | Openshift namespace                | ArgoCD Infra                                        | overlay in jobsearch-importers-infra |
|-----------------------------|------------------------------------|-----------------------------------------------------|--------------------------------------|
| onprem-prod                 | jobsearch-importers           | onprem/jobsearch-importer-prod.yaml            | onprem-prod                          |
| onprem-prod                 | jobsearch-historical-importer | onprem/jobsearch-historical-importer-prod.yaml | historical-onprem-prod               |
