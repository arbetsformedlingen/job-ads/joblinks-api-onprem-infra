# Configuration for deployment of JobAd Links API in onprem openshift clusters

 ## This repository contains files regarding build and deployment
pipelines for the
[joblinks search API repository](https://gitlab.com/arbetsformedlingen/job-ads/joblinks-search-apis/).


## Openshift Secrets that must be created:
`db-account-api`
ES_USER     username for opensearch with read access
ES_PWD      password
